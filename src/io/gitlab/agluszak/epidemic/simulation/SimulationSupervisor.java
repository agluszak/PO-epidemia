package io.gitlab.agluszak.epidemic.simulation;

import io.gitlab.agluszak.epidemic.agents.Agent;
import io.gitlab.agluszak.epidemic.agents.GregariousAgent;
import io.gitlab.agluszak.epidemic.agents.NormalAgent;
import io.gitlab.agluszak.epidemic.report.AbstractReport;
import io.gitlab.agluszak.epidemic.stats.DailyStat;
import io.gitlab.agluszak.epidemic.util.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * Nadzorca symulacji, który odpowiada za jej przeprowadzenie i stworzenie raportu.
 */
public class SimulationSupervisor {
    private final double agentBeingGregariousChance;
    private final int daysOfSimulationCount;
    private final double meetingDesireChance;
    private final double infectionChance;
    private final double mortality;
    private final double recoveryChance;
    private final int agentsCount;
    private final int averageNumberOfFriends;
    private final long seed;
    private final AbstractReport report;
    private final ArrayList<Agent> agents;
    private final Random random;
    private int currentDay;

    public SimulationSupervisor(double agentBeingGregariousChance, int daysOfSimulationCount,
                                double meetingDesireChance, double infectionChance, double mortality,
                                double recoveryChance, int agentsCount, int averageNumberOfFriends,
                                long seed, AbstractReport report) {
        this.agentBeingGregariousChance = agentBeingGregariousChance;
        this.daysOfSimulationCount = daysOfSimulationCount;
        this.meetingDesireChance = meetingDesireChance;
        this.infectionChance = infectionChance;
        this.mortality = mortality;
        this.recoveryChance = recoveryChance;
        this.agentsCount = agentsCount;
        this.averageNumberOfFriends = averageNumberOfFriends;
        this.seed = seed;
        agents = new ArrayList<>(agentsCount);
        random = new Random(seed);
        currentDay = 1;
        this.report = report;
    }

    private void setupReport() {
        report.setAgentsCount(agentsCount);
        report.setAgentBeingGregariousChance(agentBeingGregariousChance);
        report.setAverageNumberOfFriends(averageNumberOfFriends);
        report.setDaysOfSimulationCount(daysOfSimulationCount);
        report.setInfectionChance(infectionChance);
        report.setMeetingDesireChance(meetingDesireChance);
        report.setMortality(mortality);
        report.setRecoveryChance(recoveryChance);
        report.setSeed(seed);
        agents.forEach(agent -> report.addAgentInfo(agent.getAgentInfo()));
    }

    public void setupSimulation() {
        createAgents();
        chooseTheInfectedOne();
        makeFriends();
        setupReport();
    }

    private int getHealthyAgentsCount() {
        int count = 0;
        for (Agent agent : agents) {
            if (agent.isAlive() && !agent.isInfected() && !agent.isImmune()) {
                count++;
            }
        }
        return count;
    }

    private int getImmuneAgentsCount() {
        int count = 0;
        for (Agent agent : agents) {
            if (agent.isAlive() && agent.isImmune()) {
                count++;
            }
        }
        return count;
    }

    private int getInfectedAgentsCount() {
        int count = 0;
        for (Agent agent : agents) {
            if (agent.isAlive() && agent.isInfected()) {
                count++;
            }
        }
        return count;
    }

    public void runSimulation() {
        while (currentDay <= daysOfSimulationCount) {
            int healthyAgentsCount = getHealthyAgentsCount();
            int immuneAgentsCount = getImmuneAgentsCount();
            int infectedAgentsCount = getInfectedAgentsCount();
            report.logDailyStat(new DailyStat(healthyAgentsCount, infectedAgentsCount, immuneAgentsCount, currentDay));
            for (Agent agent : agents) {
                if (agent.isAlive()) {
                    agent.checkHealth();
                }
            }
            for (Agent agent : agents) {
                if (agent.isAlive()) {
                    agent.planMeetings(currentDay);
                }
            }
            for (Agent agent : agents) {
                if (agent.isAlive()) {
                    agent.takePartInMeetings(currentDay);
                }
            }
            currentDay++;
        }
        int healthyAgentsCount = getHealthyAgentsCount();
        int immuneAgentsCount = getImmuneAgentsCount();
        int infectedAgentsCount = getInfectedAgentsCount();
        report.logDailyStat(new DailyStat(healthyAgentsCount, infectedAgentsCount, immuneAgentsCount, currentDay));
    }

    public AbstractReport getReport() {
        return report;
    }

    public int getCurrentDay() {
        return currentDay;
    }

    private Agent createAgent(int id) {
        Agent agent;
        if (random.nextDouble() < agentBeingGregariousChance) {
            agent = new GregariousAgent(id, meetingDesireChance, infectionChance, mortality, recoveryChance,
                    daysOfSimulationCount, random);
        } else {
            agent = new NormalAgent(id, meetingDesireChance, infectionChance, mortality, recoveryChance,
                    daysOfSimulationCount, random);
        }
        return agent;
    }

    private void makeFriends() {
        HashSet<Pair<Agent>> friends = new HashSet<>();
        int neededPairs = (averageNumberOfFriends * agentsCount) / 2;
        while (friends.size() < neededPairs) {
            int firstId = random.nextInt(agentsCount);
            int secondId = random.nextInt(agentsCount);
            Agent firstAgent = agents.get(firstId);
            Agent secondAgent = agents.get(secondId);
            Pair<Agent> pair = new Pair<>(firstAgent, secondAgent);
            Pair<Agent> pairInverted = new Pair<>(secondAgent, firstAgent);
            if (firstAgent != secondAgent && !friends.contains(pair) && !friends.contains(pairInverted)) {
                friends.add(pair);
                firstAgent.addFriend(secondAgent);
                secondAgent.addFriend(firstAgent);
            }
        }
    }

    private void chooseTheInfectedOne() {
        int infectedId = random.nextInt(agents.size());
        agents.get(infectedId).becomeInfected();
    }

    private void createAgents() {
        for (int currentId = 1; currentId <= agentsCount; currentId++) {
            Agent agent = createAgent(currentId);
            agents.add(currentId - 1, agent);
        }
    }

    public double getInfectionChance() {
        return infectionChance;
    }

    public Random getRandom() {
        return random;
    }
}
