package io.gitlab.agluszak.epidemic.simulation;

import io.gitlab.agluszak.epidemic.agents.Agent;

import java.util.Random;

/**
 * Zaplanowane spotkanie
 */
public class Meeting {
    private final Agent invitingAgent;
    private final Agent invitedAgent;
    private final int day;
    private final double infectionChance;
    private final Random random;

    public Meeting(Agent invitingAgent, Agent invitedAgent, int day, double infectionChance, Random random) {
        this.invitingAgent = invitingAgent;
        this.invitedAgent = invitedAgent;
        this.day = day;
        this.infectionChance = infectionChance;
        this.random = random;
    }

    public Agent getInvitingAgent() {
        return invitingAgent;
    }

    public Agent getInvitedAgent() {
        return invitedAgent;
    }

    public long getDay() {
        return day;
    }

    public void takePlace() {
        if (invitingAgent.isAlive() && invitedAgent.isAlive()) {
            if (invitingAgent.isInfected() && !invitedAgent.isImmune()) {
                if (random.nextDouble() < infectionChance) {
                    invitedAgent.becomeInfected();
                }
            } else if (!invitingAgent.isImmune() && invitedAgent.isInfected()) {
                if (random.nextDouble() < infectionChance) {
                    invitingAgent.becomeInfected();
                }
            }
        }
    }
}
