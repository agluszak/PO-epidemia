package io.gitlab.agluszak.epidemic.simulation;

/**
 * Stan zdrowia agenta.
 */
public enum HealthStatus {
    HEALTHY,
    INFECTED,
    IMMUNE
}
