package io.gitlab.agluszak.epidemic.simulation;

import io.gitlab.agluszak.epidemic.agents.Agent;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Random;

/**
 * Plan spotkań dla danego agenta
 */
public class MeetingsPlan {
    private static Comparator<Meeting> comparator;
    private PriorityQueue<Meeting> queue;
    private Random random;

    static {
        comparator = Comparator.comparing(Meeting::getDay);
    }

    public MeetingsPlan(Random random) {
        queue = new PriorityQueue<>(comparator);
        this.random = random;
    }

    public void addMeeting(Agent invitingAgent, Agent invitedAgent, int day, double infectionChance) {
        Meeting meeting = new Meeting(invitingAgent, invitedAgent, day, infectionChance, random);
        queue.add(meeting);
    }

    public void letMeetingsTakePlace(int day) {
        while (queue.peek() != null && queue.peek().getDay() == day) {
            queue.poll().takePlace();
        }
    }
}
