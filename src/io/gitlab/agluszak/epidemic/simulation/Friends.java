package io.gitlab.agluszak.epidemic.simulation;

import io.gitlab.agluszak.epidemic.agents.Agent;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Zbiór znajomych wzbogacony o funkcjonalność brania losowego znajomego i zwracania listy id znajomych
 */
public class Friends {
    private Set<Agent> friendsSet;
    private Random random;

    public Friends(Random random) {
        this.random = random;
        friendsSet = new HashSet<>();
    }

    public void addFriend(Agent agent) {
        friendsSet.add(agent);
    }

    public Set<Agent> getFriendsSet() {
        return Collections.unmodifiableSet(friendsSet);
    }

    public List<Integer> getFriendsIds() {
        return friendsSet.stream().map(Agent::getId).collect(Collectors.toList());
    }

    public boolean isFriendsSetEmpty() {
        return friendsSet.isEmpty();
    }

    public Agent getRandomFriend() {
        if (friendsSet.isEmpty()) {
            throw new NoSuchElementException();
        }
        int randomIndex = random.nextInt(friendsSet.size());
        int i = 0;
        for (Agent agent : friendsSet) {
            if (i == randomIndex) {
                return agent;
            }
            i++;
        }
        throw new NoSuchElementException();
    }

}
