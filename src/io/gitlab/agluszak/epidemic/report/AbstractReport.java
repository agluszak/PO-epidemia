package io.gitlab.agluszak.epidemic.report;

import io.gitlab.agluszak.epidemic.stats.AgentInfo;
import io.gitlab.agluszak.epidemic.stats.DailyStat;

import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Abstrakcyjna klasa raportu końcowego symulacji.
 * Ktoś mógłby chcieć zrobić raport wyglądający inaczej, niż ten opisany w treści zadania
 * (np. w innym języku lub w formie obrazka).
 */
public abstract class AbstractReport {
    protected final ArrayList<DailyStat> dailyStats;
    protected final ArrayList<AgentInfo> agentInfos;

    protected String contents;

    protected long seed;
    protected int agentsCount;
    protected double agentBeingGregariousChance;
    protected double meetingDesireChance;
    protected double infectionChance;
    protected double recoveryChance;
    protected double mortality;
    protected int daysOfSimulationCount;
    protected int averageNumberOfFriends;
    protected Path path;

    public AbstractReport() {
        this.dailyStats = new ArrayList<>();
        this.agentInfos = new ArrayList<>();
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public void addAgentInfo(AgentInfo agentInfo) {
        agentInfos.add(agentInfo);
    }

    public void logDailyStat(DailyStat dailyStat) {
        dailyStats.add(dailyStat);
    }

    public abstract void prepareReport();

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public void setAgentsCount(int agentsCount) {
        this.agentsCount = agentsCount;
    }

    public void setAgentBeingGregariousChance(double agentBeingGregariousChance) {
        this.agentBeingGregariousChance = agentBeingGregariousChance;
    }

    public void setMeetingDesireChance(double meetingDesireChance) {
        this.meetingDesireChance = meetingDesireChance;
    }

    public void setInfectionChance(double infectionChance) {
        this.infectionChance = infectionChance;
    }

    public void setRecoveryChance(double recoveryChance) {
        this.recoveryChance = recoveryChance;
    }

    public void setMortality(double mortality) {
        this.mortality = mortality;
    }

    public void setDaysOfSimulationCount(int daysOfSimulationCount) {
        this.daysOfSimulationCount = daysOfSimulationCount;
    }

    public void setAverageNumberOfFriends(int averageNumberOfFriends) {
        this.averageNumberOfFriends = averageNumberOfFriends;
    }

    public abstract void write();
}
