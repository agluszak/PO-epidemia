package io.gitlab.agluszak.epidemic.report;

import io.gitlab.agluszak.epidemic.stats.AgentInfo;
import io.gitlab.agluszak.epidemic.stats.DailyStat;

import java.io.IOException;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementacja raportu według specyfikacji zadania.
 */
public class Report extends AbstractReport {
    private static final String WRITE_ERROR_MESSAGE =
            "Błąd przy zapisywaniu do pliku $1";

    private static final String PARAMS_FORMAT_STRING =
            "# twoje wyniki powinny zawierać te komentarze\n" +
                    "seed={0,number,#}\n" +
                    "liczbaAgentów={1,number,#}\n" +
                    "prawdTowarzyski={2}\n" +
                    "prawdSpotkania={3}\n" +
                    "prawdZarażenia={4}\n" +
                    "prawdWyzdrowienia={5}\n" +
                    "śmiertelność={6}\n" +
                    "liczbaDni={7,number,#}\n" +
                    "śrZnajomych={8,number,#}\n" +
                    "\n";
    private static final String AGENTS_FORMAT_STRING =
            "# agenci jako: id typ lub id* typ dla chorego\n" +
                    "{0}\n";

    private static final String GRAPH_FORMAT_STRING =
            "# graf\n" +
                    "{0}\n";

    private static final String DAILY_STATS_FORMAT_STRING =
            "# liczność w kolejnych dniach\n" +
                    "{0}";

    private String formatAgentInfo(AgentInfo agentInfo) {
        String result = String.valueOf(agentInfo.getId());
        if (agentInfo.getIsInfected()) {
            result += "*";
        }
        result += " ";
        switch (agentInfo.getType()) {
            case NORMAL:
                result += "zwykły";
                break;
            case GREGARIOUS:
                result += "towarzyski";
                break;
        }
        return result;
    }

    private String prepareAgentsInfo() {
        StringBuilder stringBuilder = new StringBuilder();
        for (AgentInfo agentInfo : agentInfos) {
            stringBuilder.append(formatAgentInfo(agentInfo));
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    private String formatFriendsInfo(AgentInfo agentInfo) {
        String result = String.valueOf(agentInfo.getId());
        List<Integer> friends = agentInfo.getFriends();
        if (!friends.isEmpty()) {
            result += " ";
            result += friends.stream().map(String::valueOf).collect(Collectors.joining(" "));
        }
        return result;
    }

    private String prepareGraphInfo() {
        StringBuilder stringBuilder = new StringBuilder();
        for (AgentInfo agentInfo : agentInfos) {
            stringBuilder.append(formatFriendsInfo(agentInfo));
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    private String formatDailyStat(DailyStat dailyStat) {
        return String.format("%d %d %d", dailyStat.getHealthyAgents(),
                dailyStat.getInfectedAgents(), dailyStat.getImmuneAgents());
    }

    private String prepareDailyStats() {
        StringBuilder stringBuilder = new StringBuilder();
        for (DailyStat dailyStat : dailyStats) {
            stringBuilder.append(formatDailyStat(dailyStat));
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public void prepareReport() {
        String paramsInfo = MessageFormat.format(PARAMS_FORMAT_STRING, seed, agentsCount,
                agentBeingGregariousChance, meetingDesireChance, infectionChance, recoveryChance,
                mortality, daysOfSimulationCount, averageNumberOfFriends);
        String agentsInfo = MessageFormat.format(AGENTS_FORMAT_STRING, prepareAgentsInfo());
        String graphInfo = MessageFormat.format(GRAPH_FORMAT_STRING, prepareGraphInfo());
        String dailyStats = MessageFormat.format(DAILY_STATS_FORMAT_STRING, prepareDailyStats());
        contents = paramsInfo + agentsInfo + graphInfo + dailyStats;
    }

    public void write() {
        try {
            Files.write(path, contents.getBytes());
        } catch (IOException e) {
            System.out.println(WRITE_ERROR_MESSAGE.replace("$1", path.toString()));
        }
    }
}
