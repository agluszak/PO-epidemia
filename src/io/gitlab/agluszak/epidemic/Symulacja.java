package io.gitlab.agluszak.epidemic;

import io.gitlab.agluszak.epidemic.config.ConfigLoader;
import io.gitlab.agluszak.epidemic.report.AbstractReport;
import io.gitlab.agluszak.epidemic.report.Report;
import io.gitlab.agluszak.epidemic.simulation.SimulationSupervisor;

import java.nio.file.Path;

public class Symulacja {

    public static void main(String[] args) {
        ConfigLoader configLoader = new ConfigLoader();
        configLoader.loadConfig();
        if (configLoader.hasErrors()) {
            System.out.println(configLoader.getErrorMessage());
            return;
        }
        double agentBeingGregariousChance = configLoader.getAgentBeingGregariousChance();
        int daysOfSimulationCount = configLoader.getDaysOfSimulationCount();
        double meetingDesireChance = configLoader.getMeetingDesireChance();
        double infectionChance = configLoader.getInfectionChance();
        double mortality = configLoader.getMortality();
        double recoveryChance = configLoader.getRecoveryChance();
        int agentsCount = configLoader.getAgentsCount();
        int averageNumberOfFriends = configLoader.getAverageNumberOfFriends();
        long seed = configLoader.getSeed();
        Path reportFilePath = configLoader.getReportFilePath();

        AbstractReport report = new Report();
        report.setPath(reportFilePath);
        SimulationSupervisor supervisor = new SimulationSupervisor(agentBeingGregariousChance, daysOfSimulationCount,
                meetingDesireChance, infectionChance, mortality, recoveryChance,
                agentsCount, averageNumberOfFriends, seed, report);

        supervisor.setupSimulation();
        supervisor.runSimulation();
        report.prepareReport();
        report.write();
    }
}
