package io.gitlab.agluszak.epidemic.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.nio.channels.Channels;
import java.nio.charset.MalformedInputException;
import java.nio.charset.StandardCharsets;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.InvalidPropertiesFormatException;
import java.util.Objects;
import java.util.Properties;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Klasa odpowiedzialna za wczytywanie i parsowanie danych potrzebnych do przeprowadzenia symulacji.
 */
public class ConfigLoader {
    private static final String DEFAULT_PROPERTIES_FILE_NAME = "default.properties";
    private static final String SIMULATION_CONF_FILE_NAME = "simulation-conf.xml";

    private static final String SEED_PROPERTY_NAME = "seed";
    private static final String AGENTS_COUNT_PROPERTY_NAME = "liczbaAgentów";
    private static final String AGENT_BEING_GREGARIOUS_CHANCE_PROPERTY_NAME = "prawdTowarzyski";
    private static final String MEETING_DESIRE_CHANCE_PROPERTY_NAME = "prawdSpotkania";
    private static final String INFECTION_CHANCE_PROPERTY_NAME = "prawdZarażenia";
    private static final String RECOVERY_CHANCE_PROPERTY_NAME = "prawdWyzdrowienia";
    private static final String MORTALITY_PROPERTY_NAME = "śmiertelność";
    private static final String DAYS_OF_SIMULATION_COUNT_PROPERTY_NAME = "liczbaDni";
    private static final String AVERAGE_NUMBER_OF_FRIENDS_PROPERTY_NAME = "śrZnajomych";
    private static final String REPORT_FILE_PATH_PROPERTY_NAME = "plikZRaportem";

    private static final String FILE_NOT_FOUND_ERROR_MESSAGE = "Brak pliku $1";
    private static final String FILE_IS_NOT_A_TEXT_FILE_ERROR_MESSAGE = "$1 nie jest tekstowy";
    private static final String FILE_IS_NOT_AN_XML_FILE_ERROR_MESSAGE = "$1 nie jest XML";
    private static final String VALUE_NOT_FOUND_ERROR_MESSAGE = "Brak wartości dla klucza $1";
    private static final String VALUE_NOT_ALLOWED_ERROR_MESSAGE = "Niedozwolona wartość \"$1\" dla klucza $2";
    private static final String VALUE_PARSING_ERROR_MESSAGE
            = "Niespodziewany błąd przy wczytywaniu wartości dla klucza $1";
    private static final String UNKNOWN_ERROR_MESSAGE = "Niespodziewany błąd przy wczytywaniu pliku $1";
    private static final String AVERAGE_NUMBER_OF_FRIENDS_GREATER_THAN_AGENTS_COUNT_ERROR_MESSAGE
            = AVERAGE_NUMBER_OF_FRIENDS_PROPERTY_NAME + " jest większa lub równa " + AGENTS_COUNT_PROPERTY_NAME;

    private long seed;
    private int agentsCount;
    private double agentBeingGregariousChance;
    private double meetingDesireChance;
    private double infectionChance;
    private double recoveryChance;
    private double mortality;
    private int daysOfSimulationCount;
    private int averageNumberOfFriends;
    private final String rootPath;
    private Path reportFilePath;
    private Properties defaultProperties;
    private Properties simulationConf;
    private String errorMessage;
    private boolean hasErrors;

    public ConfigLoader() {
        rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        defaultProperties = new Properties();
        simulationConf = new Properties();
        hasErrors = false;
    }

    static private Integer parsePositiveInteger(String s) {
        try {
            Integer value = Integer.valueOf(s);
            if (value >= 1) {
                return value;
            } else {
                return null;
            }
        } catch (NumberFormatException e) {
            return null;
        }
    }

    static private Long parseLong(String s) {
        try {
            return Long.valueOf(s);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    static private Integer parseNonNegativeInteger(String s) {
        try {
            Integer value = Integer.valueOf(s);
            if (value >= 0) {
                return value;
            } else {
                return null;
            }
        } catch (NumberFormatException e) {
            return null;
        }
    }

    static private Double parseDouble(String s) {
        try {
            return Double.valueOf(s);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    static private Double parseProbability(String s) {
        Double value = parseDouble(s);
        if (Objects.isNull(value)) {
            return null;
        } else {
            if (value <= 1 && value >= 0) {
                return value;
            } else {
                return null;
            }
        }
    }

    static private Path parsePath(String s) {
        try {
            return Paths.get(s);
        } catch (InvalidPathException e) {
            return null;
        }
    }

    private void setErrorMessageWithOneParam(String errorMessage, String param) {
        String message = errorMessage.replace("$1", param);
        setErrorMessage(message);
    }

    private void setErrorMessageWithTwoParams(String errorMessage, String firstParam, String secondParam) {
        String message = errorMessage.replace("$1", firstParam).replace("$2", secondParam);
        setErrorMessage(message);
    }

    public boolean hasErrors() {
        return hasErrors;
    }

    private void loadDefaultProperties() {
        if (hasErrors) {
            return;
        }
        try (FileInputStream stream = new FileInputStream(rootPath + DEFAULT_PROPERTIES_FILE_NAME);
             Reader reader = Channels.newReader(stream.getChannel(), StandardCharsets.UTF_8.name())) {
            defaultProperties.load(reader);
        } catch (MalformedInputException e) {
            setErrorMessageWithOneParam(FILE_IS_NOT_A_TEXT_FILE_ERROR_MESSAGE, DEFAULT_PROPERTIES_FILE_NAME);
        } catch (FileNotFoundException e) {
            setErrorMessageWithOneParam(FILE_NOT_FOUND_ERROR_MESSAGE, DEFAULT_PROPERTIES_FILE_NAME);
        } catch (IOException e) {
            setErrorMessageWithOneParam(UNKNOWN_ERROR_MESSAGE, DEFAULT_PROPERTIES_FILE_NAME);
        }
    }

    private void loadSimulationConf() {
        if (hasErrors) {
            return;
        }
        try (FileInputStream stream = new FileInputStream(rootPath + SIMULATION_CONF_FILE_NAME)) {
            simulationConf.loadFromXML(stream);
        } catch (InvalidPropertiesFormatException e) {
            setErrorMessageWithOneParam(FILE_IS_NOT_AN_XML_FILE_ERROR_MESSAGE, SIMULATION_CONF_FILE_NAME);
        } catch (FileNotFoundException e) {
            setErrorMessageWithOneParam(FILE_NOT_FOUND_ERROR_MESSAGE, SIMULATION_CONF_FILE_NAME);
        } catch (IOException e) {
            setErrorMessageWithOneParam(UNKNOWN_ERROR_MESSAGE, SIMULATION_CONF_FILE_NAME);
        }
    }

    /**
     * Próbuje wczytać parametr o nazwie propertyName typu U (najpierw z pliku defaults, potem z simulation-conf)
     * i konwertuje go przy użyciu funkcji converter, a następnie ustawia pole wskazywane przez funkcję setter
     * na tę wartość.
     *
     * @param setter       - funkcja wskazująca pole w klasie ConfigLoader, do którego parametr ma być zapisany
     * @param converter    - funkcja konwertująca napis na obiekt typu U.
     *                       W przypadku niepowodzenia powinna zwracać null.
     * @param propertyName - nazwa parametru w plikach konfiguracyjnych
     * @param <U>          - docelowy typ parametru
     */
    private <U> void loadProperty(BiConsumer<ConfigLoader, U> setter,
                                  Function<String, U> converter, String propertyName) {
        if (hasErrors) {
            return;
        }
        boolean foundDefault = false;
        try {
            String defaultPropertyValueAsString = defaultProperties.getProperty(propertyName);
            if (!Objects.isNull(defaultPropertyValueAsString)) {
                U defaultPropertyValue = converter.apply(defaultPropertyValueAsString);
                if (Objects.isNull(defaultPropertyValue)) {
                    setErrorMessageWithTwoParams(VALUE_NOT_ALLOWED_ERROR_MESSAGE,
                            defaultPropertyValueAsString, propertyName);
                } else {
                    setter.accept(this, defaultPropertyValue);
                    foundDefault = true;
                }
            }
        } catch (Exception e) {
            setErrorMessageWithOneParam(VALUE_PARSING_ERROR_MESSAGE, propertyName);
        }

        if (hasErrors) {
            return;
        }

        try {
            String simulationPropertyValueAsString = simulationConf.getProperty(propertyName);
            if (Objects.isNull(simulationPropertyValueAsString)) {
                if (foundDefault) {
                    return;
                } else {
                    setErrorMessageWithOneParam(VALUE_NOT_FOUND_ERROR_MESSAGE, propertyName);
                }
            } else {
                U simulationConfValue = converter.apply(simulationPropertyValueAsString);
                if (Objects.isNull(simulationConfValue)) {
                    setErrorMessageWithTwoParams(VALUE_NOT_ALLOWED_ERROR_MESSAGE,
                            simulationPropertyValueAsString, propertyName);
                } else {
                    setter.accept(this, simulationConfValue);
                }
            }
        } catch (Exception e) {
            setErrorMessageWithOneParam(VALUE_PARSING_ERROR_MESSAGE, propertyName);
        }
    }

    public void loadConfig() {
        loadDefaultProperties();
        loadSimulationConf();
        loadProperty(ConfigLoader::setSeed, ConfigLoader::parseLong, SEED_PROPERTY_NAME);
        loadProperty(ConfigLoader::setAgentsCount, ConfigLoader::parsePositiveInteger, AGENTS_COUNT_PROPERTY_NAME);
        loadProperty(ConfigLoader::setAgentBeingGregariousChance, ConfigLoader::parseProbability,
                AGENT_BEING_GREGARIOUS_CHANCE_PROPERTY_NAME);
        loadProperty(ConfigLoader::setMeetingDesireChance, ConfigLoader::parseProbability,
                MEETING_DESIRE_CHANCE_PROPERTY_NAME);
        loadProperty(ConfigLoader::setInfectionChance, ConfigLoader::parseProbability, INFECTION_CHANCE_PROPERTY_NAME);
        loadProperty(ConfigLoader::setRecoveryChance, ConfigLoader::parseProbability, RECOVERY_CHANCE_PROPERTY_NAME);
        loadProperty(ConfigLoader::setMortality, ConfigLoader::parseProbability, MORTALITY_PROPERTY_NAME);
        loadProperty(ConfigLoader::setDaysOfSimulationCount, ConfigLoader::parsePositiveInteger,
                DAYS_OF_SIMULATION_COUNT_PROPERTY_NAME);
        loadProperty(ConfigLoader::setAverageNumberOfFriends, ConfigLoader::parseNonNegativeInteger,
                AVERAGE_NUMBER_OF_FRIENDS_PROPERTY_NAME);
        loadProperty(ConfigLoader::setReportFilePath, ConfigLoader::parsePath, REPORT_FILE_PATH_PROPERTY_NAME);
        validate();
    }

    private void validate() {
        if (averageNumberOfFriends >= agentsCount) {
            setErrorMessage(AVERAGE_NUMBER_OF_FRIENDS_GREATER_THAN_AGENTS_COUNT_ERROR_MESSAGE);
        }
    }

    public long getSeed() {
        return seed;
    }

    private void setSeed(long seed) {
        this.seed = seed;
    }

    public int getAgentsCount() {
        return agentsCount;
    }

    private void setAgentsCount(int agentsCount) {
        this.agentsCount = agentsCount;
    }

    public double getAgentBeingGregariousChance() {
        return agentBeingGregariousChance;
    }

    private void setAgentBeingGregariousChance(double agentBeingGregariousChance) {
        this.agentBeingGregariousChance = agentBeingGregariousChance;
    }

    public double getMeetingDesireChance() {
        return meetingDesireChance;
    }

    private void setMeetingDesireChance(double meetingDesireChance) {
        this.meetingDesireChance = meetingDesireChance;
    }

    public double getInfectionChance() {
        return infectionChance;
    }

    private void setInfectionChance(double infectionChance) {
        this.infectionChance = infectionChance;
    }

    public double getRecoveryChance() {
        return recoveryChance;
    }

    private void setRecoveryChance(double recoveryChance) {
        this.recoveryChance = recoveryChance;
    }

    public double getMortality() {
        return mortality;
    }

    private void setMortality(double mortality) {
        this.mortality = mortality;
    }

    public int getDaysOfSimulationCount() {
        return daysOfSimulationCount;
    }

    private void setDaysOfSimulationCount(int daysOfSimulationCount) {
        this.daysOfSimulationCount = daysOfSimulationCount;
    }

    public int getAverageNumberOfFriends() {
        return averageNumberOfFriends;
    }

    private void setAverageNumberOfFriends(int averageNumberOfFriends) {
        this.averageNumberOfFriends = averageNumberOfFriends;
    }

    public Path getReportFilePath() {
        return reportFilePath;
    }

    private void setReportFilePath(Path reportFilePath) {
        this.reportFilePath = reportFilePath;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        this.hasErrors = true;
    }
}
