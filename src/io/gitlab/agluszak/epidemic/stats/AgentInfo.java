package io.gitlab.agluszak.epidemic.stats;

import java.util.List;

/**
 * Klasa reprezentująca informacje potrzebne do stworzenia raportu w sposób niezależny od ich
 * formatowania i/lub języka
 */
public class AgentInfo {
    private final int id;
    private final boolean isInfected;
    private final AgentType type;
    private final List<Integer> friends;

    public AgentInfo(int id, boolean isInfected, AgentType type, List<Integer> friends) {
        this.id = id;
        this.isInfected = isInfected;
        this.type = type;
        this.friends = friends;
    }

    public int getId() {
        return id;
    }

    public boolean getIsInfected() {
        return isInfected;
    }

    public AgentType getType() {
        return type;
    }

    public List<Integer> getFriends() {
        return friends;
    }
}
