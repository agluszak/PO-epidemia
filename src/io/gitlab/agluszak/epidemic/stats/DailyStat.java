package io.gitlab.agluszak.epidemic.stats;

/**
 * Statystyka zawierająca ilości zdrowych, chorych i uodpornionych agentów na początek danego dnia.
 */
public class DailyStat {
    private final int healthyAgents;
    private final int infectedAgents;
    private final int immuneAgents;
    private final int day;

    public DailyStat(int healthyAgents, int infectedAgents, int immuneAgents, int day) {
        this.healthyAgents = healthyAgents;
        this.infectedAgents = infectedAgents;
        this.immuneAgents = immuneAgents;
        this.day = day;
    }

    public int getHealthyAgents() {
        return healthyAgents;
    }

    public int getInfectedAgents() {
        return infectedAgents;
    }

    public int getImmuneAgents() {
        return immuneAgents;
    }

    public int getDay() {
        return day;
    }
}
