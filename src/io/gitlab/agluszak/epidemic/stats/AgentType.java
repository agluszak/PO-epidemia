package io.gitlab.agluszak.epidemic.stats;

/**
 * Typ agenta, potrzebny tylko do odpowiedniego formatowania raportu
 */
public enum AgentType {
    NORMAL,
    GREGARIOUS
}
