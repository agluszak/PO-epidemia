package io.gitlab.agluszak.epidemic.agents;

import io.gitlab.agluszak.epidemic.simulation.Friends;
import io.gitlab.agluszak.epidemic.stats.AgentType;

import java.util.Random;
import java.util.Set;

/**
 * Agent towarzyski.
 * Może spotykać się ze znajomymi znajomych, chyba że jest chory.
 */
public class GregariousAgent extends Agent {
    public GregariousAgent(int id, double meetingDesireChance, double infectionChance, double mortality, double recoveryChance, int daysOfSimulationCount, Random random) {
        super(id, meetingDesireChance, infectionChance, mortality, recoveryChance, daysOfSimulationCount, random);
    }

    @Override
    protected boolean keepOnPlanningMeetings() {
        double meetingDesire = getRandom().nextDouble();
        return meetingDesire < getMeetingDesireChance();
    }

    @Override
    protected Friends getFriendsToMeet() {
        Set<Agent> friendsSet = getFriends();
        Friends candidatesForAMeeting = new Friends(getRandom());
        for (Agent agent : friendsSet) {
            if (agent.isAlive()) {
                candidatesForAMeeting.addFriend(agent);
                if (!isInfected()) {
                    Set<Agent> distantFriends = agent.getFriends();
                    for (Agent distantFriend : distantFriends) {
                        if (distantFriend.isAlive() && distantFriend != this) {
                            candidatesForAMeeting.addFriend(distantFriend);
                        }
                    }
                }
            }
        }
        return candidatesForAMeeting;
    }

    @Override
    protected AgentType getType() {
        return AgentType.GREGARIOUS;
    }
}
