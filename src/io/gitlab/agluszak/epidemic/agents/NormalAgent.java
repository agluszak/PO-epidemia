package io.gitlab.agluszak.epidemic.agents;

import io.gitlab.agluszak.epidemic.simulation.Friends;
import io.gitlab.agluszak.epidemic.stats.AgentType;

import java.util.Random;
import java.util.Set;

/**
 * Agent zwykły. Jeśli jest chory, ma dwa razy mniejszą chęć na spotykanie sie ze znajomymi.
 */
public class NormalAgent extends Agent {

    public NormalAgent(int id, double meetingDesireChance, double infectionChance, double mortality, double recoveryChance, int daysOfSimulationCount, Random random) {
        super(id, meetingDesireChance, infectionChance, mortality, recoveryChance, daysOfSimulationCount, random);
    }

    @Override
    protected boolean keepOnPlanningMeetings() {
        double meetingDesire = getRandom().nextDouble();
        double meetingDesireChance = getMeetingDesireChance();
        if (isInfected()) {
            return meetingDesire < (meetingDesireChance / 2);
        } else {
            return meetingDesire < meetingDesireChance;
        }
    }

    @Override
    protected Friends getFriendsToMeet() {
        Set<Agent> friendsSet = getFriends();
        Friends candidatesForAMeeting = new Friends(getRandom());
        for (Agent agent : friendsSet) {
            if (agent.isAlive()) {
                candidatesForAMeeting.addFriend(agent);
            }
        }
        return candidatesForAMeeting;
    }

    @Override
    protected AgentType getType() {
        return AgentType.NORMAL;
    }
}
