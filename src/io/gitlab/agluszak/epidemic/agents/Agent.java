package io.gitlab.agluszak.epidemic.agents;

import io.gitlab.agluszak.epidemic.simulation.Friends;
import io.gitlab.agluszak.epidemic.simulation.HealthStatus;
import io.gitlab.agluszak.epidemic.simulation.MeetingsPlan;
import io.gitlab.agluszak.epidemic.stats.AgentInfo;
import io.gitlab.agluszak.epidemic.stats.AgentType;

import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

/**
 * Bazowa klasa abstrakcyjna dla agentów.
 */
public abstract class Agent {
    protected MeetingsPlan plan;
    private HealthStatus healthStatus;
    private Friends friends;
    private int id;
    private double meetingDesireChance;
    private double infectionChance;
    private double mortality;
    private double recoveryChance;
    private int daysOfSimulationCount;
    private Random random;

    public Agent(int id, double meetingDesireChance, double infectionChance, double mortality, double recoveryChance, int daysOfSimulationCount, Random random) {
        this.id = id;
        this.meetingDesireChance = meetingDesireChance;
        this.infectionChance = infectionChance;
        this.mortality = mortality;
        this.recoveryChance = recoveryChance;
        this.random = random;
        this.daysOfSimulationCount = daysOfSimulationCount;
        isAlive = true;
        friends = new Friends(random);
        healthStatus = HealthStatus.HEALTHY;
        plan = new MeetingsPlan(random);
    }

    private boolean isAlive;

    /**
     * @return czy agent ma ochotę umawiać się na więcej spotkań
     */
    protected abstract boolean keepOnPlanningMeetings();

    /**
     * @return zbiór przyjaciół, z którymi agent może się spotkać
     */
    protected abstract Friends getFriendsToMeet();

    private int getRandomDayFromTheFuture(int currentDay) {
        return random.nextInt(daysOfSimulationCount - currentDay) + currentDay + 1;
    }

    public void planMeetings(int currentDay) {
        if (currentDay != daysOfSimulationCount) {
            Friends friendsToMeet = getFriendsToMeet();
            if (friendsToMeet.isFriendsSetEmpty()) {
                return;
            }
            while (keepOnPlanningMeetings()) {
                Agent invitedAgent = friendsToMeet.getRandomFriend();
                int day = getRandomDayFromTheFuture(currentDay);
                plan.addMeeting(this, invitedAgent, day, infectionChance);
            }
        }
    }

    public void checkHealth() {
        if (isInfected()) {
            if (random.nextDouble() < mortality) {
                die();
            } else if (random.nextDouble() < recoveryChance) {
                recover();
            }
        }
    }

    /**
     * Metoda potrzebna wyłącznie do potencjalnej i18n.
     * Logika biznesowa programu opiera się na dziedziczeniu, a nie sprawdzaniu tej wartości.
     *
     * @return Typ agenta
     */
    protected abstract AgentType getType();

    /**
     * Zwraca dane o agencie w formie niezależnej od przyjętego sposobu formatowania.
     * Metoda potrzebna do potencjalnej i18n.
     * Zdecydowałem się zrobić to tak, a nie poprzez toString w agentach,
     * ponieważ ktoś mógłby chcieć formatować sobie raport w inny sposób.
     *
     * @return obiekt klasy AgentInfo zawierający informacje o agencie potrzebne do stworzenia raportu.
     */
    public AgentInfo getAgentInfo() {
        boolean isInfected = healthStatus == HealthStatus.INFECTED;
        List<Integer> friendsIds = friends.getFriendsIds();
        AgentType type = getType();
        AgentInfo info = new AgentInfo(id, isInfected, type, friendsIds);
        return info;
    }

    public void takePartInMeetings(int day) {
        plan.letMeetingsTakePlace(day);
    }

    public boolean isInfected() {
        return healthStatus == HealthStatus.INFECTED;
    }

    public Set<Agent> getFriends() {
        return friends.getFriendsSet();
    }

    public void addFriend(Agent agent) {
        friends.addFriend(agent);
    }

    public int getId() {
        return id;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void die() {
        isAlive = false;
    }

    public void recover() {
        healthStatus = HealthStatus.IMMUNE;
    }

    public void becomeInfected() {
        if (healthStatus != HealthStatus.IMMUNE) {
            healthStatus = HealthStatus.INFECTED;
        }
    }

    protected Random getRandom() {
        return random;
    }

    protected double getMeetingDesireChance() {
        return meetingDesireChance;
    }

    public boolean isImmune() {
        return healthStatus == HealthStatus.IMMUNE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Agent)) return false;
        Agent agent = (Agent) o;
        return id == agent.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
