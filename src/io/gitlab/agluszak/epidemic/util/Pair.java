package io.gitlab.agluszak.epidemic.util;

import java.util.Objects;

/**
 * Pomocnicza klasa do reprezentowania par obiektów
 *
 * @param <T> typ obu obiektów w parze
 */
public class Pair<T> {
    private final T firstElement;
    private final T secondElement;

    public Pair(T firstElement, T secondElement) {
        this.firstElement = firstElement;
        this.secondElement = secondElement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?> pair = (Pair<?>) o;
        return Objects.equals(getFirstElement(), pair.getFirstElement()) &&
                Objects.equals(getSecondElement(), pair.getSecondElement());
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstElement, secondElement);
    }

    public T getFirstElement() {
        return firstElement;
    }

    public T getSecondElement() {
        return secondElement;
    }
}
